/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.data
import com.example.affirmations.R
import com.example.affirmations.model.Affirmation
/**
 * [Datasource] generates a list of [Affirmation]
 */
class Datasource() {
    fun loadAffirmations(): List<Affirmation> {
        return listOf<Affirmation>(
            Affirmation(1111, R.string.affirmation1, R.drawable.image1, R.string.description1, "agustin.kohan.7e5@itb.cat", "chayanne@gmail.com", "111111111"),
            Affirmation(1112, R.string.affirmation2, R.drawable.image2, R.string.description2, "agustin.kohan.7e5@itb.cat", "oscuridadyluz@gmail.com", "114112141"),
            Affirmation(1113, R.string.affirmation3, R.drawable.image3, R.string.description3, "agustin.kohan.7e5@itb.cat", "victolanga@gmail.com", "999999999"),
            Affirmation(1114, R.string.affirmation4, R.drawable.image4, R.string.description4, "agustin.kohan.7e5@itb.cat", "molomogollon@gmail.com", "113515171"),
            Affirmation(1115,R.string.affirmation5, R.drawable.image5, R.string.description5, "agustin.kohan.7e5@itb.cat", "mondongo@gmail.com", "121536171"),
            Affirmation(1116,R.string.affirmation6, R.drawable.image6, R.string.description6, "agustin.kohan.7e5@itb.cat", "elcr7@gmail.com", "777777777"),
            Affirmation(1117,R.string.affirmation7, R.drawable.image7, R.string.description7, "agustin.kohan.7e5@itb.cat", "joesmith@gmail.com", "515151515"),
            Affirmation(1118,R.string.affirmation8, R.drawable.image8, R.string.description8, "agustin.kohan.7e5@itb.cat", "blablabla@gmail.com", "112112124"),
            Affirmation(1119, R.string.affirmation9, R.drawable.image9, R.string.description9, "agustin.kohan.7e5@itb.cat", "apruebameporfavor@gmail.com", "123431241"),
            Affirmation(1110, R.string.affirmation10, R.drawable.image10, R.string.description10, "agustin.kohan.7e5@itb.cat", "nosemeocurrenada@gmail.com", "514171819"))
    }
}
