package com.example.affirmations

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.theme.DetailViewModel

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val affirmationID = DetailViewModel(intent.getIntExtra("affirmationId", 0))

        setContent {
            AffirmationsTheme {
                DetailApp(affirmationID)
            }
        }

    }
}

@Composable
fun DetailApp(detailViewModel: DetailViewModel = viewModel()) {

    val context = LocalContext.current
    val uiState by detailViewModel.uiState.collectAsState()

    Column(
        horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier
            .padding(8.dp)
            .verticalScroll(
                rememberScrollState()
            )
    ) {
        //Image
        Image(
            painter = painterResource(id = uiState.imageResourceId),
            contentDescription = "Imagen",
            modifier = Modifier
                .size(300.dp, 200.dp)
        )
        Spacer(modifier = Modifier.padding(16.dp))
        //Title
        Text(
            text = stringResource(id = uiState.stringResourceId),
            fontSize = 25.sp,
            fontWeight = FontWeight(800)
        )
        Spacer(modifier = Modifier.padding(8.dp))
        //Descripción
        Text(
            text = stringResource(id = uiState.descriptionResourceId), textAlign = TextAlign.Justify
        )
        Spacer(modifier = Modifier.padding(8.dp))
        //2 buttons
        Row(modifier = Modifier.fillMaxWidth(1f), horizontalArrangement = Arrangement.End) {
            IconButton(onClick = { context.sendMail(uiState.mailToString, uiState.mailSubjectString) }, modifier = Modifier.padding(end = 8.dp)) {
                Icon(
                    imageVector = Icons.Filled.Mail,
                    contentDescription = "Boton para acceder al mail"
                )
            }

            IconButton(onClick = { context.dial(uiState.phoneString) }) {
                Icon(imageVector = Icons.Filled.Phone, contentDescription = "Boton para llamar")
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    AffirmationsTheme {
        DetailApp(DetailViewModel(1))
    }
}

fun Context.sendMail(to: String, subject: String){
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        startActivity(Intent.createChooser(intent, ""))
    }catch (e: ActivityNotFoundException){

    } catch (t: Throwable){

    }
}

fun Context.dial(phone: String){
    try {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        startActivity(intent)
    } catch (t: Throwable){

    }
}

