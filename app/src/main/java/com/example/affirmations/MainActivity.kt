/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.theme.MainViewModel
import com.example.affirmations.ui.theme.Typography


//private const val TAG = "MainActivity"

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        //Log.d(TAG, "onCreate Called")
        super.onCreate(savedInstanceState)
        setContent {
            // TODO 5. Show screen
            AffirmationApp()
        }
    }

    /*
    override fun onStart() {
        Log.d(TAG, "onStart Called")
        super.onStart()
    }

    override fun onResume() {
        Log.d(TAG, "onResume Called")
        super.onResume()
    }

    override fun onPause() {
        Log.d(TAG, "onPause Called")
        super.onPause()
    }

    override fun onStop() {
        Log.d(TAG, "onStop Called")
        super.onStop()
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy Called")
        super.onDestroy()
    }
    */
}

@Composable
fun AffirmationApp(gameViewModel: MainViewModel = viewModel()) {
    // TODO 4. Apply Theme and affirmation list

    val uiState by gameViewModel.uiState.collectAsState()
    AffirmationsTheme() {
        val count = rememberSaveable {
            mutableStateOf(0)
        }

        Column() {
            AffirmationList(affirmationList = uiState)
        }
    }
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
    // TODO 3. Wrap affirmation card in a lazy column
    LazyColumn(modifier = modifier) {
        items(affirmationList) { affirmation ->
            AffirmationCard(
                affirmation = affirmation, modifier = Modifier
                    .padding(8.dp)
                    .fillMaxSize()
            )
        }
    }

}

@Composable
fun OnButton(
    expanded: Boolean, onClick: () -> Unit, modifier: Modifier = Modifier

) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.primary,
            contentDescription = "Expand Button"/*stringResource(R.string.expand_button_content_description)*/
        )
    }
}

@Composable
fun CardInfo(modifier: Modifier = Modifier, description: String, affirmationId: Int) {
    Column(
        modifier = modifier.padding(
            start = 16.dp, top = 8.dp, bottom = 16.dp, end = 16.dp
        )
    ) {
        Text(
            text = "Descripción",
            fontSize = 24.sp,
            fontWeight = FontWeight(1000)
        )
        Text(
            text = description,
            fontSize = 14.sp
        )

        val affirmationContext = LocalContext.current
        Button(
            onClick = {
                affirmationContext.startActivity(
                    Intent(
                        affirmationContext,
                        DetailActivity::class.java
                    ).putExtra("affirmationId", affirmationId)
                )
            },
            modifier = Modifier
                .padding(8.dp)
                .align(alignment = Alignment.End)
        ) {
            Text(text = "Mas información")
        }
    }
}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
    var expanded by remember { mutableStateOf(false) }

    val color by animateColorAsState(
        targetValue = if (expanded) Color.Cyan else MaterialTheme.colors.surface,
    )
    // TODO 1. Your card UI
    Card(
        modifier = modifier,
        elevation = 15.dp
    ) {

        Column(
            modifier = Modifier
                .background(color = color)
                .animateContentSize(
                    animationSpec = spring(
                        dampingRatio = Spring.DampingRatioMediumBouncy,
                        stiffness = Spring.StiffnessLow
                    )
                )
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    modifier = Modifier
                        .padding(8.dp)
                        .size(64.dp),
                    painter = painterResource(id = affirmation.imageResourceId),
                    contentDescription = stringResource(id = affirmation.stringResourceId),
                    contentScale = ContentScale.Crop
                )
                Text(
                    text = stringResource(id = affirmation.stringResourceId),
                    modifier = Modifier
                        .padding(8.dp)
                        .weight(1f),
                    style = Typography.body1
                )
                OnButton(
                    expanded = expanded,
                    onClick = { expanded = !expanded },
                    modifier = modifier
                )
            }
            if (expanded) {
                CardInfo(
                    modifier = modifier,
                    stringResource(affirmation.descriptionResourceId),
                    affirmation.id
                )
            }
        }
    }

}

@Preview
@Composable
private fun AffirmationCardPreview() {
    // TODO 2. Preview your card
    AffirmationCard(
        affirmation = Affirmation(
            1,
            R.string.affirmation1,
            R.drawable.image1,
            R.string.description1,
            "mailSubject",
            "mailTo",
            "123456789"
        )
    )
}
