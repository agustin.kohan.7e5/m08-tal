package com.example.affirmations.ui.theme

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class DetailViewModel(affirmationID: Int) : ViewModel() {
    private val currentAffirmation = setCurrentAffirmation(affirmationID)

    // Game UI state
    private val _uiState = MutableStateFlow(currentAffirmation)

    // Backing property to avoid state updates from other classes
    val uiState: StateFlow<Affirmation> = _uiState.asStateFlow()



    private fun setCurrentAffirmation(affirmationID: Int) : Affirmation{
        val allAffirmations = Datasource().loadAffirmations()
        for (Affirmation in allAffirmations){
            if (Affirmation.id == affirmationID)
                return Affirmation
        }
        return allAffirmations[0]
    }

}